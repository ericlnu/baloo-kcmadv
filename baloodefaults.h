#ifndef BALOODEFAULTS_H
#define BALOODEFAULTS_H

#include <QStringList>

class BalooDefaults
{
public:
    BalooDefaults();
    ~BalooDefaults();

    static QStringList defaultExcludeFilterList();
    static QStringList defaultExcludeMimetypes();
};

#endif // BALOODEFAULTS_H
