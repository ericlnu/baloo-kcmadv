project(baloo)

find_package(KDE4 REQUIRED)
include(KDE4Defaults)

FIND_PACKAGE(Qt4 REQUIRED)
INCLUDE(${QT_USE_FILE})

include_directories(
  ${CMAKE_BINARY_DIR}
  ${QT_INCLUDES}
  ${QJSON_INCLUDE_DIR}
  ${KFILEMETADATA_INCLUDE_DIR}
  ${XAPIAN_INCLUDE_DIR}
  ${KDE4_INCLUDES}
)

add_definitions(${KDE4_ENABLE_EXCEPTIONS})



set(kcm_file_SRCS baloodefaults.cpp
  kcm.cpp
  folderselectionmodel.cpp
  indexfolderselectiondialog.cpp
)

qt4_add_dbus_interface(kcm_file_SRCS ${KDE4_DBUS_INTERFACES_DIR}/org.kde.baloo.file.indexer.xml baloofileindexerinterface)

kde4_add_ui_files(kcm_file_SRCS
  configwidget.ui
  indexfolderselectionwidget.ui
)

kde4_add_plugin(kcm_baloofileadv ${kcm_file_SRCS})

target_link_libraries(kcm_baloofileadv
  ${KDE4_KDEUI_LIBS}
  ${KDE4_KIO_LIBS}
  ${KDE4_SOLID_LIBS}
)



install(FILES kcm_baloofileadv.desktop DESTINATION ${SERVICES_INSTALL_DIR})
install(TARGETS kcm_baloofileadv DESTINATION ${PLUGIN_INSTALL_DIR})

add_subdirectory(doc)